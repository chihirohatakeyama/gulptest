<?php include_once("../include/config.php"); ?>
<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<title><?php echo $PAGE_TITLE_01 ?>｜<?php echo $SITE_TITLE ?></title>
    <?php include_once("../include/meta.php"); ?>
</head>


<body>

	<header class="contact_header">
		<?php include_once("../include/header.php"); ?>
	</header>

	<section class="well bg_contact_2">
	<div class="container">
		<h1 class="subtitle_type4">プライバシーポリシー</h1>
	</div><!-- /container -->
	</section>

	<div id="wrapper" class="privacy">
		<section class="well">
			<div class="cont-size-small">
				<h2 class="h2"><?php echo ($privacy_01_title); ?></h2>
				<div class="section9">
					<?php echo ($privacy_01_cont); ?>
				</div>
			</div>
		</section>
		<section class="well bg-gray">
			<div class="cont-size-small">
				<h2 class="h2"><?php echo ($privacy_02_title); ?></h2>
				<div class="section9">
					<?php echo ($privacy_02_cont); ?>
				</div>
			</div>
		</section>
		<section class="well">
			<div class="cont-size-small">
				<h2 class="h2"><?php echo ($privacy_03_title); ?></h2>
				<div class="section9">
					<?php echo ($privacy_03_cont); ?>
				</div>
			</div>
		</section>
		<section class="well bg-gray">
			<div class="cont-size-small">
				<h2 class="h2"><?php echo ($privacy_04_title); ?></h2>
				<div class="section9">
					<?php echo ($privacy_04_cont); ?>
				</div>
			</div>
		</section>

		<div class="well">
			<div class="cont-size-small">
				<p class="text-center"><a href="#" onClick="window.close(); return false;"><span class="text-link">ウィンドウを閉じる</span></a></p>
			</div>
		</div>
	</div><!-- /wrapper -->

    <?php include_once("../include/footer.php"); ?>
    <?php include_once("../include/js.php"); ?>
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
</body>
</html>