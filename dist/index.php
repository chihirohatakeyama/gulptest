<?php include_once("include/config.php"); ?>
<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <title>
  <?php echo $SITE_TITLE ?> </title>
  <?php include_once("include/meta.php"); ?>
  <?php include_once("include/js.php"); ?> </head>
<body>
  <header>
    <?php include_once("include/header.php"); ?> </header>
  <div id="wrapper">
    <div class="main-visual">
      <div class="media-cont__text">
        <h1 class="text1">炎上補償サービス_test</h1>
        <p class="text2">インターネット上の評判・口コミ・SNSを24時間365日監視し、<span class="indent__pc"></span>ネガティブ情報が投稿がされた際には随時ご報告、解決策のご提案をいたします。_test</p>
        <div class="row cont-size-xx-small">
          <div class="col-sm-4">
            <section class="text-center">
              <h3 class="h3"><span class="circle-sym">Webリスク対策<br>強化</span></h3>
            </section>
          </div>
          <div class="col-sm-4">
            <section class="text-center">
              <h3 class="h3"><span class="circle-sym">マーケティング<br>活用にも効果的</span></h3>
            </section></div>
          <div class="col-sm-4">
            <section class="text-center">
              <h3 class="h3"><span class="circle-sym">企業ブランド<br>価値向上</span></h3>
            </section>
          </div>
        </div>
        <p class="titlebtn"><a href="http://www.zeal-hosyou.com/contact/">今すぐ無料相談する<i class="fas fa-chevron-circle-right"></i></a></p>
      </div>
      <div class="main-visual__filter"></div>
      <div class="mv-switch__pc">
        <video class="media-cont__movie" autoplay muted playsinline loop>
          <source src="<?php echo $DOCUMENT_ROOT_URL ?>video/mv.mp4" type="video/mp4" />
          <p>video要素がサポートされていません。</p>
        </video>
      </div>
    </div>
    <!-- /media-cont__text -->
  </div>
  <!-- /main-visual -->
  <section class="well section2">
    <p class="subtitle_b">炎上補償サービスが<span class="indent"></span>選ばれる<span class="indent__pc"></span><span class="subtitle_b-emphasis">3</span>つの理由</p>
    <div class="row cont-size">
      <div class="col-sm-4 margin-bottom-large">
        <section class="text-center"><span class="circle-sym"><img src="images/section2_01.png" alt="あらゆるリスクに対応できる監視力"></span>
          <h3 class="h3">あらゆるリスクに<br>対応できる監視力</h3>
          <p class="text">24時間365日、常時監視を行っているため早期発見が可能です。<span class="indent__pc"></span>また、<span>危険性の高いサブキーワードを無制限に設定</span>することができ、あらゆるリスクを想定した監視を行います。</p>
        </section>
      </div>
      <div class="col-sm-4 margin-bottom-large">
        <section class="text-center"><span class="circle-sym"><img src="images/section2_02.png" alt="発見・報告・解決までトータルサポート"></span>
          <h3 class="h3">発見・報告・解決まで<br>トータルサポート</h3>
          <p class="text">風評対策の老舗企業としての実績から、お客様に応じた最適なサポートが可能です。<span class="indent__pc"></span>リスク情報に対し迅速に対応することで<span>企業ブランドの価値を守ります。</span></p>
        </section>
      </div>
      <div class="col-sm-4 margin-bottom-large">
        <section class="text-center"><span class="circle-sym"><img src="images/section2_03.png" alt="継続しやすい低価格万が一のときも安心"></span>
          <h3 class="h3">継続しやすい低価格<br>万が一のときも安心</h3>
          <p class="text">継続的にご活用していただけるよう、低価格でのサービス提供を行っています。<span class="indent__pc"></span>1キーワード月額4万からの対策で、もしもの際に<span>通常費用がかかる風評対策（総額最大120万）を無償で行えます。</span></p>
        </section>
      </div>
    </div>
    <!-- /row -->
    <!-- /container -->
  </section>
  <section class="well section3 bg_trouble">
    <div class="cont-size1000">
      <div class="row section3_inner">
        <p class="subtitle_b">このような課題は<span class="indent"></span>ありませんか？</p>
        <div class="section3_inner_left">
          <dl><dt><i class="far fa-check-square fa-fw"></i></dt>
            <dd>自社商品やサービス・店舗に<span class="strong">悪い評判</span>がないか確認したい</dd>
          </dl>
          <dl><dt><i class="far fa-check-square fa-fw"></i></dt>
            <dd>従業員がSNSに<span class="strong">不適切な書き込み</span>を行っていないか気になる</dd>
          </dl>
        </div>
        <div class="section3_inner_left">
          <dl><dt><i class="far fa-check-square fa-fw"></i></dt>
            <dd>リテラシー研修を行っているが<span class="strong">実践できているか気になる</span></dd>
          </dl>
          <dl><dt><i class="far fa-check-square fa-fw"></i></dt>
            <dd><span class="strong">退職者</span>による会社評価の書き込みを抑制したい</dd>
          </dl>
        </div>
        <div class="section3_inner_left">
          <dl><dt><i class="far fa-check-square fa-fw"></i></dt>
            <dd>ネット上で何かあったときの<span class="strong">相談相手がいない</span></dd>
          </dl>
          <dl><dt><i class="far fa-check-square fa-fw"></i></dt>
            <dd>自社のリソースでは<span class="strong">監視しきれない</span></dd>
          </dl>
        </div>
      </div>
    </div>
  </section>
  <section class="well">
    <div class="container_inner cont-size-small">
      <p class="subtitle_c">炎上補償サービスなら<br>解決できます</p>
      <p class="subtitle_d">お問い合わせ・ご質問等は、<br>お電話またはメールフォームよりお気軽にご連絡ください。</p>
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 bg- wow bounceIn" data-wow-delay="0.3s">
          <div class="btn-inquiry"><a href="tel:0364335704">
                <span class="btn-inquiry__tel"><i class="fas fa-phone-volume fa-fw"></i>03-6433-5704</span>
                <span>受付時間&nbsp;平日9:00～18:00<div class="indent"></div>（土日・祝日除く）</span>
              </a> </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 bg- wow bounceIn" data-wow-delay="0.3s">
          <div class="btn-inquiry"> <a href="<?php echo $DOCUMENT_ROOT_URL ?>contact/">
                  <span class="btn-inquiry__enquiry"><i class="fas fa-envelope fa-fw"></i>メールフォームからの<spam class="indent"></spam>お問い合わせ</span>
                </a> </div>
        </div>
      </div>
    </div>
  </section>
  <section class="well section5 bg-gray">
    <div class="cont-size">
      <p class="subtitle mt20"><span>SERVICE</span></p>
      <h2 class="h2">監視からコンサルティング<span class="indent"></span>までのフロー</h2>
      <div class="img_pc"><img src="images/section5_00.png" alt="監視からコンサルティングまでのフロー"></div>
      <div class="img_sp"><img src="images/section5_00_sp.png" alt="監視からコンサルティングまでのフロー"></div>
      <div class="box">
        <p class="title">1.キーワード設定</p>
        <div class="box_inner">
          <div class="img"><img src="../images/section5_01.png" alt="キーワード設定"></div>
          <div class="discription_0">
            <p class="discription">メインで監視するキーワードとは別に、危険性の高いサブキーワードを合わせて監視します。<br><span>初期設定で約250種類のサブキーワード</span>が登録されていますが、お客様のご希望に合わせて、<span>無制限にカスタマイズ可能</span>です。</p>
            <p class="discription_2">監視の対象となるメインキーワードは、<strong>「会社名」「店舗名」「サービス名」</strong>などを設定されるお客様が多いです。</p>
          </div>
        </div>
      </div>
      <div class="box">
        <p class="title">2.監視</p>
        <p class="discription_type2">24時間365日、常時システム監視を行います。<br><span>検索時のキーワード</span>や<span>Webサイト</span>から、<span>TwitterやFacebookなどのSNS</span>の投稿状況まで、ネット上の情報を一括して監視します。</p>
        <div class="row box_inner_type2">
          <div class="col-sm-4">
            <p class="heading">候補キーワード監視</p> <img src="images/section5_02.png" alt="候補キーワード監視">
            <p class="text"><span>「Yahoo!キーワード」と「Googleサジェスト」</span>を対象とし、それぞれにネガティブキーワードが表示された際に非表示対策をします。</p>
          </div>
          <div class="col-sm-4">
            <p class="heading">検索結果コンテンツ監視</p> <img src="images/section5_03.png" alt="検索結果コンテンツ監視">
            <p class="text">対象キーワードを検索した際の検索結果（最大100位）までの<span>Webサイトにある口コミ情報</span>を監視し、原則週次でご報告いたします。</p>
          </div>
          <div class="col-sm-4">
            <p class="heading">SNS監視</p> <img src="images/section5_04.png" alt="SNS監視">
            <p class="text"><span>「Twitter」と「Facebook」</span>を監視します。問題ありと判断したものは投稿日時、投稿内容、アカウント名を控え、ご報告します。</p>
          </div>
        </div>
      </div>
      <div class="box">
        <p class="title">3.レポート</p>
        <p class="discription_type2">各項目を日別での状況にまとめた<span>「週次レポート」</span>、弊社が分析した炎上事例をまとめた<span>「月次ネット炎上レポート」</span>をお送りします。<br>また、万が一監視範囲にサブキーワードが発見された場合は<span>リアルタイムでご報告</span>をいたします。</p>
        <div class="row box_inner_type2">
          <div class="col-sm-4">
            <p class="heading">リアルタイム報告</p> <img src="images/section5_05.png" alt="リアルタイム報告">
            <p class="text">万が一炎上に繋がるような投稿を発見した際、<span>原則1時間以内</span>に現状報告とその後の対応方法を明記したものになります。<br>※オプションの有無により報告スピードが異なります。</p>
          </div>
          <div class="col-sm-4">
            <p class="heading">週次レポート</p> <img src="images/section5_06.png" alt="週次レポート">
            <p class="text">毎週1週間分の投稿数、投稿内容などの詳細をまとめた報告書をお送り致します。<br>全ての投稿を抽出するので<span>マーケティングとしてご活用</span>されるお客様もいます。</p>
          </div>
          <div class="col-sm-4">
            <p class="heading">月次レポート</p> <img src="images/section5_07.png" alt="月次レポート">
            <p class="text">弊社が実際に分析した<span>炎上事例、解決までのノウハウ</span>をまとめた資料をお送りいたします。<br>どのようなケースが炎上に繋がるのかなど、覚えておいて損はない情報が満載です。</p>
          </div>
        </div>
        <div class="merit">
          <p class="subtitle"><span>レポートは自社の業務改善に活かせる！</span></p>
          <p class="discription_type2">レポート内の口コミは社内や店舗で共有することをおすすめしています。<br>よい口コミは従業員の<strong>モチベーションアップ</strong>に繋がります。<br>また、「態度が悪かった」などのよくない口コミは、<strong>業務改善</strong>のヒントとなります。<br><strong>サービス向上や顧客満足度アップにぜひレポートをお役立てください。</strong></p>
        </div>
      </div>
    </div>
  </section>
  <div class="band">
    <div class="cont-size-small">
      <div class="band_inner_1">
        <div class="band_inner_2">
          <div class="text">
            <div class="text_1">インターネット上で</div>
            <div class="text_2"><span class="under_orange">ネガティブ情報<span></div>
          </div>
          <div class="text_3">発見</div>
        </div>
        <div class="band_inner_3"><img src="images/woman1.png" alt="インターネット上でネガティブ情報発見"></div>
      </div>
    </div>
  </div>


  <section class="well section5 bg-gray">
    <div class="cont-size">

    <div class="box">
      <p class="title">4.分析</p>
      <div class="box_inner">
      <div class="img"><img src="images/section5_08.png" alt="分析"></div>
      <p class="discription_3">ネガティブ情報を発見した場合は、弊社基準（拡散性の判断・想定される影響・書き込みの悪質性・事実、風評の判断）に基づいたリスク分析を行い、対策が必要かどうかを判断します。</p>
    </div>
    </div>

    <div class="box">
      <p class="title">5.コンサルティング</p>
      <p class="discription_type2">リスク分析後、ネガティブ情報への対策方法をご提案します。<br>削除または非表示にできる場合は、<span>弊社ノウハウにより非表示対策</span>を行います。<br>削除または非表示にできない場合でも、<span>炎上の可能性を最小限に収める方法</span>をご提案します。場合によりお客様に直接ご対応頂くことがございますが、原則弊社が対応します。</p>
              <div class="row box_inner_type2">
                <div class="col-sm-4">
                  <p class="heading">候補キーワード対策イメージ</p>
                  <div class="mb60"><img src="images/section5_11.png" alt="候補キーワード対策イメージ"></div>
                </div>
                <div class="col-sm-4">
                  <p class="heading">検索結果コンテンツ対策イメージ</p>
                  <div class="mb60"><img src="images/section5_09.png" alt="検索結果コンテンツ対策イメージ"></div>
                </div>
                <div class="col-sm-4">
                  <p class="heading">SNS対策イメージ</p>
                  <div class="mb60"><img src="images/section5_10.png" alt="SNS対策イメージ"></div>
                </div>
              </div>
            </div>
          </div>
          </section>
          <div class="band_type2">
            <div class="band_inner cont-size-small">
              <div class="text">スピーディに<span class="indent"></span><span>沈静化</span>します</div>
              <div class="img"><img src="images/woman2.png" alt="スピーディに沈静化します"></div>
            </div>
          </div>
          <section class="well section6">
            <div class="container_inner cont-size-x-small">
              <p class="subtitle_d">炎上時の対策は万全ですか？</p>
              <p class="subtitle_c">炎上補償サービスなら<br><span>専門知識をもったプロ</span>が<br><span>ネットリスクから貴社をお守りします！</span></p>
            </div>
          </section>
          <section class="well bg_contact">
            <div class="container_inner cont-size-small">
              <p class="subtitle mt20"><span>Contact US</span></p>
              <h2 class="h2">お問い合わせ</h2>
              <p class="subtitle_d">お電話・メールフォームにてお気軽にお問い合わせください</p>
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 bg- wow bounceIn" data-wow-delay="0.3s">
                  <div class="btn-inquiry"><a href="tel:0364335704">
                <span class="btn-inquiry__tel"><i class="fas fa-phone-volume fa-fw"></i>03-6433-5704</span>
                <span>受付時間&nbsp;平日9:00～18:00<div class="indent"></div>（土日・祝日除く）</span>
              </a> </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 bg- wow bounceIn" data-wow-delay="0.3s">
                  <div class="btn-inquiry"> <a href="<?php echo $DOCUMENT_ROOT_URL ?>contact/">
                  <span class="btn-inquiry__enquiry"><i class="fas fa-envelope fa-fw"></i>メールフォームからの<spam class="indent"></spam>お問い合わせ</span>
                </a> </div>
                </div>
              </div>
            </div>
          </section>
          <section class="well section7">
            <p class="subtitle mt20"><span>PRICE</span></p>
            <h2 class="h2">ご利用料金</h2>
            <div class="cont-size-x-small">
              <div class="section7_inner_1">
                <div class="text1">1検索キーワードあたりの月額料金</div>
                <div class="text2">
                  <div class="fee2">40,000<span class="fee3">円 ～</span></div>
                </div>
              </div>
              <p style="margin-top: 30px;font-size: 15px;">・リアルタイム報告は、弊社の全営業時間が対象となります（平日 : 9:00～18:00）<br>
                 ・弊社営業時間外に抽出したネガティブ情報に関しては上記レポート頻度（毎週・隔週・毎月）に報告致します</p>
              <div class="section7_inner_2">
                <div>
                  <p class="subtitle_e">お支払い方法</p>
                  <p class="text">お支払い方法は口座引落（毎月20日）となります</p>
                </div>
                <div>
                  <p class="subtitle_e">注意事項</p>
                  <p class="text">以下に該当する場合、本サービスのお申込みができませんのでご注意下さい。<br><br>1. 弊社審査により、本サービスのお申し込みをお断りする場合がございます。 <br>2. 監視範囲内の状況により、本サービスの申し込みができない場合もございます。</p>
                </div>
              </div>
            </div>
          </section>
          <section class="well section10">
      <div class="section10_inner cont-size1000">
    <p class="subtitle "><span>FLOW</span></p>
    <h2 class="h2 ">導入から運用までの流れ</h2>
    <div class="flow row ">
    <div class="col-md-2 col-xs-12 flow_inner bg">
      <div class="text1 ">1</div>
      <div><i class="fas fa-search "></i></div>
      <div class="text2 ">キーワード選定</div>
    </div>
    <div class="col-md-2 col-xs-12 flow_inner bg">
      <div class="text1 ">2</div>
      <div><i class="fas fa-check-circle "></i></div>
      <div class="text2 ">お申込み</div>
    </div>
    <div class="col-md-2 col-xs-12 flow_inner bg">
      <div class="text1 ">3</div>
      <div><i class="fas fa-eye "></i></div>
      <div class="text2 ">監視開始</div>
    </div>
    <div class="col-md-2 col-xs-12 flow_inner bg">
      <div class="text1 ">4</div>
      <div><i class="fas fa-file-alt "></i></div>
      <div class="text2 ">レポート提出</div>
    </div>
    <div class="col-md-2 col-xs-12 flow_inner bg">
      <div class="text1 ">5</div>
      <div><i class="fas fa-exclamation-triangle"></i></div>
      <div class="text2 ">炎上発見</div>
    </div>
    <div class="col-md-2 col-xs-12 flow_inner">
      <div class="text1 ">6</div>
      <div><i class="fas fa-comments "></i></div>
      <div class="text2 ">コンサルティング</div>
    </div>
    </div>
  </div>
  </section>


  <div class="band_type3">
    <div class="band_inner cont-size-small">
    <div class="text ">約<span>2600社</span>様の<span class="indent"></span>運用実績があります</div>
  </div>
  </div>


  <section class="well section8">
    <div class="cont-size-x-small">
    <h2 class="h2 ">業種別導入実績</h2>
    <div class="cont-size ">
      <div class="row ">
        <div class="col-md-2 col-xs-6">
          <span class="circle-sym "><i class="fas fa-utensils"></i></span>
          <div class="text1 ">飲食店<br>食品メーカー</div>
          <div class="text2 ">457社</div>
        </div>
        <div class="col-md-2 col-xs-6">
          <span class="circle-sym "><i class="fas fa-home"></i></span>
          <div class="text1 ">建築・リフォーム<br>分譲・賃貸</div>
          <div class="text2 ">317社</div>
        </div>
        <div class="col-md-2 col-xs-6">
          <span class="circle-sym "><i class="fas fa-graduation-cap"></i></span>
          <div class="text1 ">学校法人・その他</div>
          <div class="text2 ">279社</div>
        </div>
        <div class="col-md-2 col-xs-6">
          <span class="circle-sym "><i class="fas fa-shopping-cart"></i></span>
          <div class="text1 ">専門店・EC</div>
          <div class="text2 ">243社</div>
        </div>
        <div class="col-md-2 col-xs-6">
          <span class="circle-sym "><i class="fas fa-medkit"></i></span>
          <div class="text1 ">病院・介護・福祉</div>
          <div class="text2 ">229社</div>
        </div>
        <div class="col-md-2 col-xs-6">
          <span class="circle-sym "><i class="fas fa-tv"></i></span>
          <div class="text1 ">メディア</div>
          <div class="text2 ">174社</div>
        </div>
      </div>

      <div class="row ">
        <div class="col-md-2 col-xs-6">
          <span class="circle-sym "><i class="fas fa-users"></i></span>
          <div class="text1 ">人材</div>
          <div class="text2 ">87社</div>
        </div>
        <div class="col-md-2 col-xs-6">
          <span class="circle-sym "><i class="fas fa-dollar-sign"></i></span>
          <div class="text1 ">金融・保険</div>
          <div class="text2 ">68社</div>
        </div>
        <div class="col-md-2 col-xs-6">
          <span class="circle-sym "><i class="fas fa-cubes"></i></span>
          <div class="text1 ">レジャー</div>
          <div class="text2 ">61社</div>
        </div>
        <div class="col-md-2 col-xs-6">
          <span class="circle-sym "><i class="fas fa-suitcase"></i></span>
          <div class="text1 ">ホテル・旅館</div>
          <div class="text2 ">55社</div>
        </div>
        <div class="col-md-2 col-xs-6">
          <span class="circle-sym "><i class="fas fa-recycle"></i></span>
          <div class="text1 ">エネルギー<br>リサイクル</div>
          <div class="text2 ">50社</div>
        </div>

        <div class="col-md-2 col-xs-6">
          <span class="circle-sym "><i class="fas fa-truck"></i></span>
          <div class="text1 ">流通・運送</div>
          <div class="text2 ">43社</div>
        </div>
      </div>


    </div>
  </div>
  </section>

  <section class="well">
    <div class="cont-size-small">
    <h2 class="h2">お客様の声</h2>
    <div class="section9">
    <article class="voice">
        <div class="img"><img src="images/section9_01.jpg" alt="お客様の声（飲食店）"></div>
        <div class="text">
          <h3 class="voice_title">
            <div class="title1">飲食店</div>
            <div class="title2">「信頼できる対応で安心しました」</div>
          </h3>
          <div class="voice_text">元々<span>離職率が高い業界</span>なので、ある程度書き込みをされるのは仕方ありませんが、何かあった時に頼れるサービスがあればと考えていました。根本的な削除ができないと意味がないと感じていましたが、<span>他社の炎上事例</span>なども教えてくれて<span>今後の予防策として従業員に喚起するいい機会になっています</span>。また、ネガティブな投稿がされたときに、素早く報告してくれたのが印象的でした。</div>
        </div>
    </article>

    <article class="voice_type2">
        <div class="img"><img src="images/section9_02.png" alt="お客様の声（化粧品）"></div>
        <div class="text">
          <h3 class="voice_title">
            <div class="title1">化粧品</div>
            <div class="title2">「経費の削減にも繋がりました」</div>
          </h3>
          <div class="voice_text">普段目視でネットの監視はおこなっていましたが、<span>人件費削減のため導入を検討しました</span>。目視では拾えなかった部分まで報告をもらい結果的に経費の削減にも繋がりました。ブランドに対しての投稿だけではなく、各店舗にはアルバイトもいるため<span>SNSへの投稿にも意識を傾けなければいけない</span>という話もあがっていたので、これを機に導入させていただきました。</div>
        </div>
    </article>

        <article class="voice">
        <div class="img"><img src="images/section9_03.jpg" alt="お客様の声（アパレル）"></div>
        <div class="text">
          <h3 class="voice_title">
            <div class="title1">アパレル</div>
            <div class="title2">「何かあったときの保険として導入しました」</div>
          </h3>
          <div class="voice_text">ネット監視は今までしておらず、初めて聞く話だったので正直不安ではありましたが、最近同業他社で炎上した会社があったため<span>他人事ではない</span>と感じていました。サービス導入後も元々投稿数が多いものではないので、今のところ問題ありそうな投稿は見当たりませんが、<span>何かあったときの保険として導入</span>したのもあるのでしばらく利用する予定です。</div>
        </div>
    </article>

    <article class="voice_type2">
        <div class="img"><img src="images/section9_04.jpg" alt="お客様の声（健康食品）"></div>
        <div class="text">
          <h3 class="voice_title">
            <div class="title1">健康食品</div>
            <div class="title2">「口コミはマーケティングとして役立てています」</div>
          </h3>
          <div class="voice_text">通販サイトにある口コミが気になっており当初は削除を希望していました。しかし、炎上する可能性があるとのことだったので今は静観しています。口コミが高い頻度で投稿される商材なので、<span>自社だけではカバーできない</span>と感じ監視を依頼しました。毎週レポートで報告がされるので管理がしやすいのと、<span>ポジティブな投稿も拾ってくれるので今はマーケティングとして役立てています。</span></div>
        </div>
    </article>

        <article class="voice">
        <div class="img"><img src="images/section9_05.jpg" alt="お客様の声（専門学校）"></div>
        <div class="text">
          <h3 class="voice_title">
            <div class="title1">専門学校</div>
            <div class="title2">「他社よりも報告やサポートが充実しています」</div>
          </h3>
          <div class="voice_text">監視は別の会社で頼んでいましたが、サービスの提案を受けて比較すると補償内容にかなりの差があったので乗り換えを決めました。気になっていた口コミサイトの内容についても<span>相談にのってくれて対策方法などを教えて頂きました</span>。特にSNS投稿については毎週細かく報告を頂いており、費用に対してのサービスは良いものだと感じています。</div>
        </div>
    </article>
  </div>
  </div>
  </section>




  <section class="well bg_contact">
      <div class="container_inner cont-size-small">
    <p class="subtitle mt20"><span>Contact US</span></p>
    <h2 class="h2 ">お問い合わせ</h2>
        <p class="subtitle_d">お電話・メールフォームにてお気軽にお問い合わせください</p>
        <div class="row ">
          <div class="col-lg-6 col-md-6 col-sm-6 bg- wow bounceIn">
            <div class="btn-inquiry "><a href="tel:0364335704 ">
                <span class="btn-inquiry__tel"><i class="fas fa-phone-volume fa-fw"></i>03-6433-5704</span>
                <span>受付時間&nbsp;平日9:00～18:00<div class="indent "></div>（土日・祝日除く）</span>
              </a> </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 bg- wow bounceIn">
            <div class="btn-inquiry"> <a href="<?php echo $DOCUMENT_ROOT_URL ?>contact/"> <span class="btn-inquiry__enquiry"><i class="fas fa-envelope fa-fw"></i>メールフォームからの<spam class="indent"></spam>お問い合わせ</span> </a>
        </div>
      </div>
    </div>
  </div>
  </section>
  <section class="well">
    <p class="subtitle mt20"><span>COMPANY</span></p>
    <h2 class="h2">会社概要</h2>
    <section id="company__profile" class="jumbotron bg-white">
      <div class="cont-size900">
        <?php echo ($outline_zc_lp_outline); ?>
      </div>
      <?php /*
      <div class="row cont-size900">
        <div class="col-sm-6">
          <dl class="dl-responsive"> <dt>会社名</dt>
            <dd>株式会社ジールコミュニケーションズ</dd>
            <dd class="link"><a href="http://www.zeal-c.jp/" target="_blank">zeal-c.jp<i class="fas fa-external-link-alt"></i></a></dd>
          </dl>
          <dl class="dl-responsive"> <dt>所在地</dt>
            <dl>
              <dd class="margin-bottom-small">東京本社</dd>
              <dd>
                <?php echo $data["postal_code_tokyo"] ?><br /> 東京都渋谷区東2-16-10 ヤナセ渋谷ビル4F・5F（受付 4F）<a href="https://goo.gl/maps/yFazGXaef752" target="_blank" class="blank"> <span class="indent__pc"></span><span class="link">GoogleMap<i class="fas fa-external-link-alt"></i></span> </a><br /> TEL :
                <?php echo $data["tel_tokyo"] ?><span class="indent"></span>FAX :
                <?php echo $data["fax_tokyo"] ?><br /> 受付時間 9:00 ～ 18:00<span class="reception">（土日・祝日除く） </span></dd>
            </dl>
            <dl>
              <dd class="margin-bottom-small">大阪支店</dd>
              <dd>
                <?php echo $data["postal_code_osaka"] ?><br />
                <?php echo $data["address_osaka"] ?><br /> TEL :
                <?php echo $data["tel_osaka"] ?><span class="indent"></span>FAX :
                <?php echo $data["fax_osaka"] ?><br /> 受付時間 9:00 ～ 18:00<span class="reception">（土日・祝日除く） </span></dd>
            </dl>
          </dl>
        </div>
        <div class="col-sm-6">
          <dl class="dl-responsive"> <dt>代表者</dt>
            <dd>代表取締役
              <?php echo $data["ceo_name"] ?> </dd>
          </dl>
          <dl class="dl-responsive"> <dt>資本金</dt>
            <dd>1億1,500万円（グループ連結）</dd>
          </dl>
          <dl class="dl-responsive"> <dt>従業員数</dt>
            <dd>170名（2018年4月現在 グループ全体）</dd>
          </dl>
          <dl class="dl-responsive"> <dt>事業内容</dt>
            <dd>Webリスクコンサルティング事業</dd>
            <dd>Webマーケティング事業</dd>
          </dl>
        </div>
      </div>
      */ ?>
    </section>
  </section>

  <section class="well bg_contact">
    <div class="row bnr cont-size">
      <div class="p-2-s-5">
        <div> <a href="http://www.zeal-c.jp/" target="_blank">
              <img src="images/bnr_zc.png" alt="株式会社ジールコミュニケーションズバナー">
            </a> </div>
      </div>
      <div class="p-2-s-5">
        <div> <a href="https://www.zeal-security.jp/" target="_blank">
              <img src="images/bnr_service.png" alt="株式会社ジールコミュニケーションズサービスサイトバナー">
            </a> </div>
      </div>
      <div class="p-2-s-5">
        <div> <a href="http://www.zeal-sns.com/" target="_blank">
              <img src="images/bnr_sns.png" alt="SNS運用サービスバナー">
            </a> </div>
      </div>
            <div class="p-2-s-5">
        <div> <a href="https://repyuken.com/" target="_blank">
              <img src="images/bnr_repyuken.png" alt="レピュ研バナー">
            </a> </div>
      </div>
            <div class="p-2-s-5">
        <div> <a href="http://www.reysol.co.jp/" target="_blank">
              <img src="images/bnr_sponsor.png" alt="柏レイソルバナー">
            </a> </div>
      </div>
    </div>
  </section>


  <?php include_once("include/footer.php"); ?>
</body>
</html>