<?php include_once("../include/config.php"); ?>
<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<title><?php echo $PAGE_TITLE_02 ?>｜<?php echo $SITE_TITLE ?></title>
    <?php include_once("../include/meta.php"); ?>
</head>


<body>
<header class="contact_header">
  <?php include_once("../include/header.php"); ?>
</header>

<section class="well bg_contact_2 ptb30 mb50">
      <div class="container">
      <p class="subtitle_type4">お問い合わせ送信完了</p>
      </div><!-- /container -->
    </section>  

  <div id="wrapper">


     <section class="well bg-white">
        <div class="cont-size-x-small">
        	<p>お問い合わせいただきましてありがとうございます。<br />おって弊社より3営業日以内にご返信致しますので、しばしお待ち頂けますようお願いいたします。</p>

          <p>確認のため、ご入力されたメールアドレスに確認メールを送らせていただいております。<br />
          万が一3営業日以上返信がない場合は、誠に恐縮ですが再度メールアドレスをご確認の上、<br />お問い合わせフォームよりお問い合わせ下さいますようお願い申し上げます。</p>
        </div>
          <p class="margin-top-large wow bounceIn mb50" data-wow-delay="0.5s"><a href="<?php echo $DOCUMENT_ROOT_URL ?>" class="btn btn-middle btn-success" role="button">トップページへ戻る</a></p>
        
    </section><!-- /well -->

    <?php include_once("../include/footer.php"); ?>
    <?php include_once("../include/js.php"); ?>
  </div><!-- /wrapper -->

</body>
</html>