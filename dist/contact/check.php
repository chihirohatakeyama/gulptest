<?php
include("../include/config.php");



session_start();
$_SESSION["company_name"] = htmlspecialchars($_POST["company_name"]);
$_SESSION["name"] = htmlspecialchars($_POST["name"]);
$_SESSION["tel"] = mb_convert_kana(htmlspecialchars($_POST["tel"]), 'a');
$_SESSION["email"] = mb_convert_kana(htmlspecialchars($_POST["email"]), 'a');
$_SESSION["kind_01"] = htmlspecialchars($_POST["kind_01"]);
$_SESSION["kind_02"] = htmlspecialchars($_POST["kind_02"]);
$_SESSION["kind_03"] = htmlspecialchars($_POST["kind_03"]);
$_SESSION["kind_04"] = htmlspecialchars($_POST["kind_04"]);
$_SESSION["inquiry"] = htmlspecialchars($_POST["inquiry"]);




$error_frag = false;




// 名前
if( empty($_SESSION["name"]) ) {
	$error_frag  = true;
	$error_m02   = "<small>！未入力です</small>";
	$cautions_02 = " class=\"cautions\"";
}
else { $error_m02 = ""; }


// メールアドレス
if( empty($_SESSION["email"]) ) {
	$error_frag  = true;
	$error_m03   = "<small>！未入力です</small>";
	$cautions_03 = " class=\"cautions\"";
}
else if ( !preg_match("/([\w\d_-]+@[\w\d_-]+\.[\w\d._-]+)/",$_SESSION["email"]) ) {
	$error_frag  = true;
	$error_m03   = "<small>！入力内容が正しくありません</small>";
	$cautions_03 = " class=\"cautions\"";
}
else { $error_m03 = ""; }


// 電話番号
if( empty($_SESSION["tel"]) ) {
	$error_frag  = true;
	$error_m04   = "<small>！未入力です</small>";
	$cautions_04 = " class=\"cautions\"";
}
else { $error_m04 = ""; }


// お問い合わせ種類
if ( empty($_SESSION["kind_01"]) and empty($_SESSION["kind_02"]) and empty($_SESSION["kind_03"]) and empty($_SESSION["kind_04"]) ) {
	$error_frag  = true;
	$error_m05   = "<strong>！未選択です</strong>";
	$cautions_05 = " cautions";
}
else { $error_m05 = ""; }


// お問い合わせ内容
if( empty($_SESSION["inquiry"]) ) {
	$error_frag  = true;
	$error_m06   = "<small>！未入力です</small>";
	$cautions_06 = " class=\"cautions\"";
}
else { $error_m06 = ""; }








if($error_frag == true) {
	require_once("tpl/index.tpl.php");
}

else if ($error_frag == false) {
	require_once("tpl/confirm.tpl.php");
}

?>