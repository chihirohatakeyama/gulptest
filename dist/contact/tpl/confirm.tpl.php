<?php include_once("../include/config.php"); ?>
<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<title><?php echo $PAGE_TITLE_02 ?>｜<?php echo $SITE_TITLE ?></title>
    <?php include_once("../include/meta.php"); ?>
</head>

<body>
  <div id="wrapper">
<header class="contact_header">
  <?php include_once("../include/header.php"); ?>



</header>

<section class="well bg_contact_2 ptb30 mb50">
      <div class="container">
      <p class="subtitle_type4">お問い合わせ内容確認</p>
      </div><!-- /container -->
    </section>  

     <div class="well bg-white">
      <div class="cont-size-small">

      	<form action="<?php echo $DOCUMENT_ROOT_URL ?>contact/send-mail.php" method="post" id="mail-form"　enctype="multipart/form-data">

			<dl class="form-list">
				<dt><span class="form-list__icon">必須</span>ご担当者様名</dt>
				<dd><?php echo $_SESSION["name"] ?></dd>
			</dl>

			<dl class="form-list">
				<dt>貴社名</dt>
				<dd><?php echo $_SESSION["company_name"] ?></dd>
			</dl>			

			<dl class="form-list">
				<dt><span class="form-list__icon">必須</span>メールアドレス</dt>
				<dd><?php echo $_SESSION["email"] ?></dd>
			</dl>

			<dl class="form-list">
				<dt><span class="form-list__icon">必須</span>お電話番号</dt>
				<dd><?php echo $_SESSION["tel"] ?></dd>
			</dl>

			<dl class="form-list">
					<dt><span class="form-list__icon">必須</span>お問い合わせ種類</dt>
					<dd>
						<ul>
<?php
$kind_list = array(
"".$_SESSION["kind_01"]."",
"".$_SESSION["kind_02"]."",
"".$_SESSION["kind_03"]."",);
$kind_list = array_merge(array_diff($kind_list, array("")));

foreach ( $kind_list as $key => $value ) {

?>
						<li>■ <?php echo $value ?></li>
<?php } ?>
					</ul>
				</dd>
			</dl>

			<dl class="form-list">
				<dt><span class="form-list__icon">必須</span>お問い合わせ内容</dt>
				<dd><textarea name="inquiry" id="inquiry" /><?php echo $_SESSION["inquiry"] ?></textarea></dd>
			</dl>

			<div class="localnav">
				<ul>
					<li class="localnav__list"><a href="<?php echo $DOCUMENT_ROOT_URL ?>contact/">戻って修正</a></li>
					<li class="localnav__list button form-01"><input type="submit" value="送信" /></li>
				</ul>
			</div>
		</form>
 
      </div><!-- /container -->
    </div><!-- /well -->

    <?php include_once("../include/footer.php"); ?>
    <?php include_once("../include/js.php"); ?>
  </div><!-- /wrapper -->
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
</body>
</html>