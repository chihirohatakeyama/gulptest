<?php include_once("../include/config.php"); ?>
<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<title><?php echo $PAGE_TITLE_01 ?>｜<?php echo $SITE_TITLE ?></title>
    <?php include_once("../include/meta.php"); ?>
</head>


<body>

<header class="contact_header">
  <?php include_once("../include/header.php"); ?>
</header>

<section class="well bg_contact_2">
      <div class="container">
      <p class="subtitle_type4">お問い合わせ</p>
      </div><!-- /container -->
</section>  


  <div id="wrapper">
     <div class="well cont-size-small">
      <div class="cont-size-small">
        <form class="form" action="<?php echo $DOCUMENT_ROOT_URL ?>contact/result.php" method="post" enctype="multipart/form-data" name="selbox">
			
			<dl class="form-list">
				<dt><label for="name"><span class="form-list__icon">必須</span>ご担当者様名</label></dt>
				<dd><input type="text" onFocus="this.select()" name="name" id="name"<?php echo $cautions_02 ?> value="<?php echo $_SESSION["name"] ?>" placeholder="例）山田 太郎" required /><?php echo $error_m02 ?></dd>
			</dl>

			<dl class="form-list">
				<dt><label for="company_name">貴社名</label></dt>
				<dd><input type="text" onFocus="this.select()" name="company_name" id="name" value="<?php echo $_SESSION["company_name"] ?>" placeholder="例）○○株式会社" /></dd>
			</dl>


			<dl class="form-list">
				<dt><label for="email"><span class="form-list__icon">必須</span>メールアドレス</label></dt>
				<dd><input type="email" onFocus="this.select()" name="email" id="email"<?php echo $cautions_03 ?> value="<?php echo $_SESSION["email"] ?>" placeholder="例）example@mail.jp"  required/><?php echo $error_m03 ?></dd>
			</dl>

			<dl class="form-list">
				<dt><label for="tel"><span class="form-list__icon">必須</span>お電話番号</label></dt>
				<dd><input type="tel" onFocus="this.select()" name="tel" id="tel"<?php echo $cautions_04 ?> value="<?php echo $_SESSION["tel"] ?>" placeholder="例）03-6433-5701" required /><?php echo $error_m04 ?></dd>
			</dl>

			<dl class="form-list">
				<dt><label for="kind"><span class="form-list__icon">必須</span>お問い合わせ種類</label></dt>
				<dd>
					<ul class="vertical <?php echo $cautions_05 ?>">
					<?php $kind_list = array("サービス内容について詳しく知りたい","資料・見積依頼","その他"); ?>
						<li><input type="checkbox" name="kind_01" id="kind_01" value="<?php echo $kind_list[0] ?>"<?php echo ($_SESSION["kind_01"] == $kind_list[0] ? " checked=\"checked\"" : "") ?>><label for="kind_01"><?php echo $kind_list[0] ?></label></li>
						<li><input type="checkbox" name="kind_02" id="kind_02" value="<?php echo $kind_list[1] ?>"<?php echo ($_SESSION["kind_02"] == $kind_list[1] ? " checked=\"checked\"" : "") ?>><label for="kind_02"><?php echo $kind_list[1] ?></label></li>
						<li><input type="checkbox" name="kind_03" id="kind_03" value="<?php echo $kind_list[2] ?>"<?php echo ($_SESSION["kind_03"] == $kind_list[2] ? " checked=\"checked\"" : "") ?>><label for="kind_03"><?php echo $kind_list[2] ?></label></li>
					</ul><?php echo $error_m05 ?>
				</dd>
			</dl>

			<dl class="form-list">
				<dt class="heightLine-group2"><label for="inquiry"><span class="form-list__icon">必須</span>お問い合わせ内容</label></dt>
				<dd class="heightLine-group2"><textarea name="inquiry" id="inquiry"<?php echo $cautions_06 ?> placeholder="内容を入力" required /><?php echo $_SESSION["inquiry"] ?></textarea><?php echo $error_m06 ?></dd>
			</dl>

			<p class="text-center__sp-left"><a href="<?php echo $DOCUMENT_ROOT_URL ?>privacy/" target="_blank"><span class="text-link">個人情報保護方針</span><span class="t-blank"></span></a>を一読のうえ、確認画面へお進みください。</p>

			<p class="button form-01"><input type="submit" value="確認画面へ" /></p>
		</form>
      </div><!-- /container -->
    </div><!-- /well -->

    <?php include_once("../include/footer.php"); ?>
    <?php include_once("../include/js.php"); ?>
  </div><!-- /wrapper -->
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
</body>
</html>