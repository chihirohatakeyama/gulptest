
	<ol class="breadcrumb">
		<div class="breadcrumb__cont">
<?php /*  TOP  */ ?>
				<li>
					<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
						<i class="fa fa-home" aria-hidden="true"></i> <a href="<?php echo $DOCUMENT_ROOT_URL ?>" itemprop="url"><span itemprop="title"><?php echo $SITE_TITLE_TOP ?></span></a>
					</div>
				</li>
<?php /*  1階層目  */ ?>
	<?php if ( !empty($PAGE_TITLE_01) ) { ?>
			<li>
					<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
						<a href="<?php echo $DOCUMENT_ROOT_URL ?><?php echo $PAGE_PATH_01 ?>" itemprop="url"><span itemprop="title"><?php echo $PAGE_TITLE_01 ?></span></a>
					</div>
				</li>	
				<li>
					<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
						<a href="<?php echo $DOCUMENT_ROOT_URL ?><?php echo $PAGE_PATH_02 ?>" itemprop="url">
							<span itemprop="title"><?php echo $PAGE_TITLE_02 ?></span>
						</a>
					</div>
				</li>
		<?php } ?>	
<?php /*  2階層目  */ ?>
	<?php if ( !empty($PAGE_TITLE_02) ) { ?>
			<li>
					<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
						<a href="<?php echo $DOCUMENT_ROOT_URL ?><?php echo $PAGE_PATH_03 ?>" itemprop="url">
							<span itemprop="title"><?php echo $PAGE_TITLE_03 ?></span>
						</a>
					</div>
				</li>
	<?php } ?>
		</div><!-- /breadcrumb__cont -->
	</ol>
