<section class="well">
      <div class="container">
       <h2 class="h2 text-center">お問い合わせ・<div class="indent"></div>ご相談はこちら<span class="border-main-color"></span></h2>
       <p class="text-center">最短5分でお見積いたします。<span class="indent"></span>お気軽にお問い合わせください。</p>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 bg- wow bounceIn" data-wow-delay="0.3s">
              <div class="btn-inquiry">
                <a href="tel:0364335701">
                <span class="btn-inquiry__tel">03-6433-5701</span>
                <span>受付時間&nbsp;平日9:00～18:00（土日・祝日除く）</span>
              </a>
              </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 bg- wow bounceIn" data-wow-delay="0.3s">
              <div class="btn-inquiry">
                <a href="<?php echo $DOCUMENT_ROOT_URL ?>contact/">
                  <span class="btn-inquiry__enquiry">お問い合わせ・<div class="indent"></div>資料請求</span>
                </a>
              </div>
          </div>
        </div><!-- /row -->
      </div><!-- /container -->
    </section>
