<meta charset="utf-8">
<meta name="description" content="<?php echo $PAGE_DESC ?>" />
<meta name="robots" content="<?php echo $ROBOTS ?>" />
<meta name="viewport" content="width=device-width,ini-tial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<!-- CSS -->
  <link rel="stylesheet" href="<?php echo $DOCUMENT_ROOT_URL ?>css/bootstrap.css">
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo $DOCUMENT_ROOT_URL ?>css/style.css">

<!-- favicon -->
<link rel="icon" href="<?php echo $DOCUMENT_ROOT_URL ?>images/favicon.ico" type="image/vnd.microsoft.icon">

<!-- web font -->
<link rel="stylesheet" href="//fonts.googleapis.com/earlyaccess/notosansjapanese.css">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

<!-- <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet"> -->

<!-- ogp -->
<meta property="og:title" content="炎上補償サービス | 株式会社ジールコミュニケーションズ" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $PAGE_URL ?>" />
<meta property="og:image" content="<?php echo $DOCUMENT_ROOT_URL ?>images/OGP.png" />
<meta property="og:site_name" content="<?php echo $SITE_TITLE ?>" />
<meta property="og:description" content="<?php echo $PAGE_DESC ?>" />

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-25874601-15"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-25874601-15');
</script>

